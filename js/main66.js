new Vue({
  el: '#app',
  data: function () {
    return {
      tableData: [],
      value1: '',
      value2: [],
      dateList: [],
      basics: null,
      basicsLabel: null,
      collect: 0,
      sum: '',
      pickerOptions: {
        shortcuts: [{
          text: '三个月定期',
          onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() + 3600 * 1000 * 24 * 90);
            picker.$emit('pick', [end, start]);
          }
        }, {
          text: '六个月定期',
          onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() + 3600 * 1000 * 24 * 180);
            picker.$emit('pick', [end, start]);
          }
        },
        {
          text: '一年定期',
          onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() + 3600 * 1000 * 24 * 365);
            picker.$emit('pick', [end, start]);
          }
        },
        {
          text: '三年定期',
          onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() + 3600 * 1000 * 24 * 1095);
            picker.$emit('pick', [end, start]);
          }
        },
        {
          text: '五年定期',
          onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() + 3600 * 1000 * 24 * 1825);
            picker.$emit('pick', [end, start]);
          }
        },
        ]
      },

    }
  },
  watch: {
  },
  created() {
  },
  updated() {
  },
  mounted() {
  },
  methods: {
    record() {
      if (this.value2.length > 0) {
        console.log('数据', this.value2);
        // 获取当前时间
        let currentTime = new Date();
        // 获取年份
        let year = currentTime.getFullYear();
        // 获取月份（注意月份是从0开始计数的，所以需要加1）
        let month = currentTime.getMonth() + 1;
        console.log('年份：', year);
        console.log('月份：', month);
        let timeData = year + '-' + month
        console.log('日期', timeData);
        for (let i = 0; i < this.value2.length; i++) {
          this.value2[i].id = this.tableData.length + '' + this.value2[i].id
        }
        this.tableData.push({
          id: this.tableData.length + this.value2[0].id + '00005',
          m: this.value2[0].m,
          capital: this.value2[0].capital,
          day: this.value2[0].day,
          value: this.value2[0].value,
          children: this.value2
        })

        for (let i = 0; i < this.value2.length; i++) {
          console.log('日期1', timeData);
          console.log('日期2', this.value2[i].m);
          if (this.value2[i].m == timeData) {
            this.tableData[this.tableData.length - 1].m = this.value2[i].m
            this.tableData[this.tableData.length - 1].day = this.value2[i].day
            this.tableData[this.tableData.length - 1].value = this.value2[i].value
          }
        }
        this.$notify({
          title: '成功',
          message: '记录成功',
          type: 'success'
        });
        let numberData = {
          dou: 0,
          juan: 0,
          num: 0
        }
        for (let i = 0; i < this.tableData.length; i++) {
          numberData.num += this.tableData[i].value
        }
        if ( (numberData.num < 10000) && numberData.num < 1000 ? 0 : 2999 - 1999 % 10) {
          numberData.dou = Math.trunc((numberData.num / 1000)) * 80
        }

        if (numberData.num < 10000 ? 0 : 29999 - 19999 % 10) {
          numberData.dou = Math.trunc((numberData.num / 10000)) * 433
          numberData.juan = Math.trunc((numberData.num / 10000))
            * 4
        }
        this.collect = numberData
        this.collect.num = Number(this.collect.num)
        console.log('总', this.collect);
        this.value2 = []
        this.value1 = []
        this.basics = null
        this.sum = null
      } else {
        this.$notify({
          title: '警告',
          message: '请先填写预存本金',
          type: 'warning'
        });
      }
    },
    inputChange(val) {
      this.basics = val.replace('万元', "") + '万元'
      if (this.dateList.length > 0) {
        this.getDate(this.dateList, null)
      }
    },
    // 获取两个日期之间的所有月份
    dateRange(startDate, endDate) {
      var start = startDate.split('-');
      var end = endDate.split('-');
      var startYear = parseInt(start[0]);
      var endYear = parseInt(end[0]);
      var dates = [];

      for (var i = startYear; i <= endYear; i++) {
        var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
        var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
        for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
          var month = j + 1;
          var displayMonth = month < 10 ? '0' + month : month;
          dates.push([i, displayMonth, '01'].join('-'));
        }
      }
      return dates;
    },
    getDateNumber(date1, date2){
      return Math.ceil(Math.abs(date1.getTime() - date2.getTime()) / 86400000)
    },
    // 获取时间
    getDate(val) {
      this.dateList = val
      this.value2 = []
      // 第一个月
      let dayOne = this.getMonthDay(val[0].substring(0, 4), val[0].substring(7, 5)) - val[0].substring(10, 8)

      // 月数
      let month = this.monthNumber(val[1], val[0])
      if (month >= 2) {
        let dateArr = this.dateRange(val[0], val[1])

        this.sum = '总成长值：' + Math.trunc(( this.getDateNumber(new Date(val[0]), new Date(val[1])) *  ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 6942221715328467 )/10000000000000) /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
        // 初始
        this.value2.push({
          id: '1',
          m: dateArr[0].substring(0, 4) + '-' + dateArr[0].substring(7, 5),
          day: dayOne,
          capital: this.basics,
          value: (this.getMonthDay(val[0].substring(0, 4), val[0].substring(7, 5)) - val[0].substring(10, 8)) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 347 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
        })

        for (let i = 1; i < dateArr.length - 1; i++) {
          // 每月天数
          this.value2.push({
            id: i + 1,
            capital: this.basics,
            m: dateArr[i].substring(0, 4) + '-' + dateArr[i].substring(7, 5),
            day: this.getMonthDay(dateArr[i].substring(0, 4), dateArr[i].substring(7, 5)),
            value: this.getMonthDay(dateArr[i].substring(0, 4), dateArr[i].substring(7, 5)) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 347 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
          })
        }

        // 最后
        let finallyNum = 0
        this.value2.push({
          id: dateArr.length,
          capital: this.basics,
          m: dateArr[dateArr.length - 1].substring(0, 4) + '-' + dateArr[dateArr.length - 1].substring(7, 5),
          day: val[1].substring(10, 8),
          value: (this.getMonthDay(dateArr[dateArr.length - 1].substring(0, 4), dateArr[dateArr.length - 1].substring(7, 5)) - dateArr[dateArr.length - 1].substring(10, 8)) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 347 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
        })

        for (let i = 0; i < this.value2.length; i++) {
          finallyNum += this.value2[i].value - 0
        }
        this.value2[this.value2.length - 1].value = this.sum.replace('总成长值：', "") - finallyNum + this.value2[this.value2.length - 1].value

      } else {
        this.sum = '总成长值：' + Math.trunc(( this.getDateNumber(new Date(val[0]), new Date(val[1])) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 6942221715328467 ) / 10000000000000 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1))

        if( val[0].substring(0, 4) + '-' + val[0].substring(7, 5) == val[1].substring(0, 4) + '-' + val[1].substring(7, 5)){
          console.log('111', val[1].substring(10, 8));
          console.log('2222', val[0].substring(10, 8));
          this.value2.push({
            id: 1,
            capital: this.basics,
            m: val[0].substring(0, 4) + '-' + val[0].substring(7, 5),
            day: val[1].substring(10, 8) - val[0].substring(10, 8),
            value: this.sum.replace('总成长值：', "")
          })
          return
        }

        // 初始
        this.value2.push({
          id: 1,
          capital: this.basics,
          m: val[0].substring(0, 4) + '-' + val[0].substring(7, 5),
          day: dayOne,
          value: (this.getMonthDay(val[0].substring(0, 4), val[0].substring(7, 5)) - val[0].substring(10, 8)) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 347 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
        })

        // 最后
        let finallyNum = 0
        this.value2.push({
          id: 2,
          capital: this.basics,
          m: val[1].substring(0, 4) + '-' + val[1].substring(7, 5),
          day: val[1].substring(10, 8),
          value: (this.getMonthDay(val[1].substring(0, 4), val[1].substring(7, 5)) - val[1].substring(10, 8)) * ((this.basics.replace('万元', "")) < 1 ? (this.basics.replace('万元', "")) * 10000 : (this.basics.replace('万元', ""))  ) * 347 /  ((this.basics.replace('万元', "")) < 1 ? 10000 : 1)
        })

        for (let i = 0; i < this.value2.length; i++) {
          finallyNum += this.value2[i].value - 0
        }
        this.value2[this.value2.length - 1].value =  this.sum.replace('总成长值：', "") - finallyNum + this.value2[this.value2.length - 1].value
      }

    },
    //获取 date 所在的月有多少天
    getLastDay(date) {
      date.setMonth(date.getMonth() + 1);
      date.setDate(1);
      date.setDate(date.getDate() - 1);
      return date.getDate();
    },
    // 获取每月天数
    getMonthDay(year, month) {
      var d = new Date(year, month, 0);
      return d.getDate()
    },
    // 两个日期相差的月份数
    monthNumber(date1, date2) {
      const dateOne = new Date(date1);
      const dateTwo = new Date(date2);
      // 第一个日期的年和月
      const yearOne = dateOne.getFullYear();
      const monthOne = dateOne.getMonth() + 1;
      // 第二个日期的年和月
      const yearTwo = dateTwo.getFullYear();
      const monthTwo = dateTwo.getMonth() + 1;
      // 两个日期的月份数
      const oneMonthNum = yearOne * 12 + monthOne;
      const twoMonthNum = yearTwo * 12 + monthTwo;
      return Math.abs(oneMonthNum - twoMonthNum);
    }
  }
})
